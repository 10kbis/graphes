#include <stdlib.h>
#include <stdio.h>
#include "graph.h"
#include "utils.h"

int main(int argc, char ** argv) {
    if (argc != 6 && argc != 7) {
        printf(
            "Invalid arguments!\n"
            "Usage: graph <m> <kn> <n> <output_file> <color_threshold> [<seed>]\n"
            "\t<m>\t\t\tNumber of edges per vertex generated.\n"
            "\t<kn>\t\t\tSize of initial graph Kn.\n"
            "\t<n>\t\t\tNumber of vertices to generate.\n"
            "\t<output_file>\t\tPath of the output graphviz file.\n"
            "\t<color_threshold>\tIf the degree of a vertex is higher than the threshold the color will be different. If 0 no color is applied.\n"
            "\t<seed>\t\t\tInitial seed.\n"
        );
        return -1;
    }

    int m = atoi(argv[1]);
    int kn = atoi(argv[2]);
    int n = atoi(argv[3]);
    char * output_file = argv[4];
    int threshold = atoi(argv[5]);

    if (argc == 7) {
        srand32(atol(argv[6]));
    }
    
    if (m <= 0) {
        printf("m must be positive.\n");
        return -1;
    }

    if (kn <= 1) {
        printf("Kn must be bigger than 1.\n");
        return -1;
    }

    if (n <= 0) {
        printf("n must be positive.\n");
        return -1;
    }

    if (m > kn) {
        printf("m must be less than kn (the initial number of vertices).\n");
        return -1;
    }

    if (threshold < 0) {
        printf("threshold must be positive or nul.\n");
        return -1;
    }

    graph_t * g = graph_create_kn(kn);

    int err = graph_evolve(g, m, n);

    if (err == -1) {
        graph_free(g);
        printf("m is too high.\n");
        return -1;
    } else if (err == -2) {
        graph_free(g);
        printf("Not enough memory.\n");
        return -1;
    }
    
    graph_save_graphviz(g, output_file, threshold);
    graph_free(g);
        
    return 0;
}
#ifndef _VECTOR_H_
#define _VECTOR_H_

#include <stddef.h>
#include <stdbool.h>

typedef void (* free_func_t)(void *);
typedef bool (* filter_func_t)(const void * elem, void * data);
typedef int (* cmp_func_t)(const void * a, const void * b);

/* Opaque structure */
typedef struct vec vec_t;


/* ------------------------------------------------------------------------- *
 * Build an empty vector.
 *
 * PARAMETERS
 * free_elem_func    A function to free each element of the vector
 *
 * NOTE
 * The returned structure should be cleaned with `vec_free` after usage
 *
 * RETURN
 * vec   The created vector, or NULL in case of error
 * ------------------------------------------------------------------------- */
vec_t * vec_create(free_func_t free_elem_func);


/* ------------------------------------------------------------------------- *
 * Free the memory allocated for the vector.
 *
 * PARAMETERS
 * vec          The vector to free
 * ------------------------------------------------------------------------- */
void vec_free(vec_t * vec);


/* ------------------------------------------------------------------------- *
 * Add an element `elem` at the end the vector `vector`
 *
 * PARAMETERS
 * vec      A valid pointer to the vector in which to add the character
 * elem     The element to add
 *
 * RETURN
 * added    `true` if the character was added. `false` in case of error
 * ------------------------------------------------------------------------- */
bool vec_add(vec_t * vec, void * elem);


/* ------------------------------------------------------------------------- *
 * Retrieve the character at index `index` for the given vector.
 *
 * PARAMETERS
 * vec      A valid pointer to the vector to retrieve from
 * index    The index of the elem. 0 <= index <= N-1
 *          where N is the size of the vector
 *
 * RETURN
 * elem     The element
 * ------------------------------------------------------------------------- */
void * vec_get(const vec_t * vec, size_t index);


/* ------------------------------------------------------------------------- *
 * Return the size of the vector (aka. the number of elements).
 *
 * PARAMETERS
 * vec   A valid pointer to a vector
 *
 * RETURN
 * size         The size of the vector
 * ------------------------------------------------------------------------- */
size_t vec_size(const vec_t * vec);

/* ------------------------------------------------------------------------- *
 * Return the size of the vector (aka. the number of elements).
 *
 * PARAMETERS
 * vec          A valid pointer to a vector
 * cmp_func     A function to compare to elements. Returns -1 when a < b,
 *              0 when a == b and 1 when a > b
 * ------------------------------------------------------------------------- */
void vec_sort(vec_t * vec, cmp_func_t cmp_func);

/* ------------------------------------------------------------------------- *
 * Remove and return the last element of the vector
 *
 * PARAMETERS
 * vec          A valid pointer to a vector
 * ------------------------------------------------------------------------- */
void * vec_pop(vec_t * vec);

/* ------------------------------------------------------------------------- *
 * Remove elements from a vector according to a predicat
 *
 * PARAMETERS
 * vec          A valid pointer to a vector
 * filter_func  A function taking as parameter a pointer to an element and
 *              returning `true` if the element must be removed and `false`
 *              otherwise
 * ------------------------------------------------------------------------- */
void vec_filter(vec_t * vec, filter_func_t filter_func, void * data, size_t max);

/* ------------------------------------------------------------------------- *
 * Remove elements from a vector according to a predicat
 *
 * PARAMETERS
 * vec          A valid pointer to a vector
 * cmp_func     A function taking as parameter a pointer to an element and elem.
 * elem         User defined data passed as a parameter to cmp_func
 * 
 * RETURN
 * True if elem is contained in vec, False otherwise
 * ------------------------------------------------------------------------- */
bool vec_contains(vec_t * vec, cmp_func_t cmp_func, void * data);

/* ------------------------------------------------------------------------- *
 * Check if a vector is empty
 *
 * PARAMETERS
 * vec          A valid pointer to a vector
 * 
 * RETURN
 * empty?   True is the vector is empty, false otherwise
 * ------------------------------------------------------------------------- */
bool vec_empty(vec_t * vec);

/* ------------------------------------------------------------------------- *
 * Copy a vector and the elements inside
 * 
 * NOTE
 * If `copy_func` is NULL the elements are not copied and point to the same
 * elements as those in the old vector. When `vec_free` is called on the new
 * vector, the elements will not be freed
 *
 * PARAMETERS
 * vec          A valid pointer to a vector
 * copy_func    A function to copy the elements.
 * 
 * RETURN
 * vector       The new vector
 * ------------------------------------------------------------------------- */
vec_t * vec_copy(vec_t * vec, void * (* copy_func)(void * elem));

/* ------------------------------------------------------------------------- *
 * Remove all elements of the vector
 * 
 * PARAMETERS
 * vec          A valid pointer to a vector
 * ------------------------------------------------------------------------- */
void vec_clear(vec_t * vec);

/* ------------------------------------------------------------------------- *
 * Remove the i-th element from the vector
 *
 * PARAMETERS
 * vec          A valid pointer to a vector
 * i            The index of the element to remove
 * ------------------------------------------------------------------------- */
void vec_remove(vec_t * vec, size_t i);

/* ------------------------------------------------------------------------- *
 * Swap the i-th element and j-th from the vector
 *
 * PARAMETERS
 * vec          A valid pointer to a vector
 * i            The index of the first element to swap
 * j            The index of the second element to swap
 * ------------------------------------------------------------------------- */
void vec_swap(vec_t * vec, size_t i, size_t j);
#endif // _VECTOR_H_

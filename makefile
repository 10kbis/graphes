.PHONY: clean web

CC:=gcc -Wall -O2 -g
EMCC:=emcc -Wall -O2 -g
WEB_EXPORTED_FUNCTIONS:=-s "EXPORTED_FUNCTIONS=[\
	'_graph_create',		\
	'_graph_free',			\
	'_graph_add_vertex',	\
	'_graph_add_edge',		\
	'_graph_evolve',		\
	'_graph_save_graphviz',	\
	'_graph_print',			\
	'_graph_create_kn',		\
	'_graph_get_nb_edges',	\
	'_graph_get_edges',		\
	'_graph_get_nb_vertices',\
	'_free'					\
]"
WEB_EXPORTED_RUNTIME:=-s "EXPORTED_RUNTIME_METHODS=['ccall', 'cwrap']"
WEB_EXPORT_FLAGS:=$(WEB_EXPORTED_FUNCTIONS) $(WEB_EXPORTED_RUNTIME)

graph: main.o graph.o utils.o vec.o
	$(CC) main.o graph.o utils.o vec.o -o graph

main.o: main.c graph.h
	$(CC) -c main.c

graph.o: graph.c graph.h utils.h
	$(CC) -c graph.c

utils.o: utils.c utils.h
	$(CC) -c utils.c

vec.o: vec.c vec.h
	$(CC) -c vec.c

web: graph.c vec.c utils.c
	$(EMCC) graph.c vec.c utils.c -o graph.js $(WEB_EXPORT_FLAGS)


clean:
	rm -f *.o *.wasm graph graph.js
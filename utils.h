#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdint.h>
#include <stdlib.h>

uint32_t rand32();
void srand32(uint32_t seed);
uint32_t randint(uint32_t min, uint32_t max);
int cmp_size_t(size_t * a, size_t * b);
#endif
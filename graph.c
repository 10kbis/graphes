#include <stdio.h>
#include <string.h>
#include "graph.h"
#include "vec.h"
#include "utils.h"

struct graph {
    uint32_t nb_vertices;
    vec_t * edges;
};

graph_t * graph_create() {
    graph_t * g = (graph_t *) malloc(sizeof(graph_t));
    if (!g) {
        return NULL;
    }

    g->nb_vertices = 0;
    g->edges = vec_create(free);

    return g;
}

void graph_free(graph_t * g) {
    vec_free(g->edges);
    free(g);
}

uint32_t graph_add_vertex(graph_t * g) {
    g->nb_vertices++;
    return g->nb_vertices;
}

bool graph_add_edge(graph_t * g, uint32_t vertex1, uint32_t vertex2) {
    // maybe check if edge between vertex1 and vertex2 is already in graph
    edge_t * e1 = (edge_t *) malloc(sizeof(edge_t));
    if (!e1) {
        return false;
    }

    edge_t * e2 = (edge_t *) (uint32_t *) malloc(sizeof(edge_t));
    if (!e2) {
        free(e1);
        return false;
    }

    e1->from_vertex = vertex1;
    e1->to_vertex = vertex2;
    e2->from_vertex = vertex2;
    e2->to_vertex = vertex1;

    if (!vec_add(g->edges, e1)) {
        free(e1);
        free(e2);
        return false;
    }

    if (!vec_add(g->edges, e2)) {
        free(e1);
        free(e2);
        // remove e1 from g->edges
        vec_pop(g->edges);
        return false;
    }

    return true;
}

static uint32_t select_random_vertex(graph_t * g) {
    uint32_t x = randint(0, vec_size(g->edges) - 1);
    edge_t * edge = (edge_t *) vec_get(g->edges, x);
    return edge->from_vertex;
}

int graph_evolve(graph_t * g, unsigned m, unsigned n) {
    // The number of connections for the new node must be lower than the
    // number of nodes in the current graph
    if (m > g->nb_vertices) {
        return -1;
    }

    vec_t * selected_vertices = vec_create(free);

    for (unsigned k = 0; k < n; k++) {
        for (uint32_t i = 0; i < m; i++) {
            bool foundNewVertex = false;
            while (!foundNewVertex) {
                uint32_t vertex = select_random_vertex(g);
                if (!vec_contains(selected_vertices, (cmp_func_t)cmp_size_t, &vertex)) {
                    foundNewVertex = true;
                    uint32_t * v = (uint32_t *) malloc(sizeof(uint32_t));
                    if (!v) {
                        vec_free(selected_vertices);
                        return -2;
                    }
                    *v = vertex;
                    if (!vec_add(selected_vertices, v)) {
                        vec_free(selected_vertices);
                        return -2;
                    }
                }
            }
        }
    
        uint32_t new_vertex = graph_add_vertex(g);

        for (uint32_t i = 0; i < vec_size(selected_vertices); i++) {
            uint32_t selected_vertex = *(uint32_t *)vec_get(selected_vertices, i);
            graph_add_edge(g, new_vertex, selected_vertex);
        }

        vec_clear(selected_vertices);
    }
    
    vec_free(selected_vertices);

    return 0;
}

void graph_save_graphviz(graph_t * g, char * path, uint32_t threshold) {
    FILE * f = fopen(path, "w");
    
    fprintf(f,
        "digraph G{\n"
        "   edge [\n"
        "       arrowhead=\"none\"\n"
        "   ];\n"
    );

    if (threshold > 0) {
        vec_t * degrees = vec_create(free);
        for (uint32_t i = 0; i < g->nb_vertices; i++) {
            uint32_t * degree = (uint32_t *) malloc(sizeof(uint32_t));
            if (!degree) {
                vec_free(degrees);
                fclose(f);
                return;
            }
            *degree = 0;
            vec_add(degrees, degree);
        }

        for (uint32_t i = 0; i < vec_size(g->edges); i++) {
            edge_t * e = (edge_t *) vec_get(g->edges, i);
            uint32_t * degree = (uint32_t *) vec_get(degrees, e->from_vertex - 1);
            *degree += 1;
        }

        for (uint32_t i = 0; i < g->nb_vertices; i++) {
            uint32_t * degree = (uint32_t *) vec_get(degrees, i);
            if (*degree >= threshold) {
                fprintf(f, "   %u [color=red];\n", i);
            }
        }

        vec_free(degrees);
    }

    for (uint32_t i = 0; i < vec_size(g->edges); i++) {
        edge_t * e = (edge_t *) vec_get(g->edges, i);
        if (e->from_vertex <= e->to_vertex) {
            fprintf(f, "    %u -> %u;\n", e->from_vertex, e->to_vertex);
        }
    }
    fprintf(f, "}\n");
    fclose(f);
}

void graph_print(graph_t * g) {
    printf(
        "digraph G{\n"
        "   edge [\n"
        "       arrowhead=\"none\"\n"
        "   ];\n"
    );

    for (uint32_t i = 0; i < vec_size(g->edges); i++) {
        edge_t * e = (edge_t *) vec_get(g->edges, i);
        if (e->from_vertex <= e->to_vertex) {
            printf("    %u -> %u;\n", e->from_vertex, e->to_vertex);
        }
    }
    printf("}\n");
}


graph_t * graph_create_kn(uint32_t n) {
    graph_t * g = graph_create();
    if (!g) {
        return NULL;
    }

    for (uint32_t i = 0; i < n; i++) {
        uint32_t v = graph_add_vertex(g);
        for (uint32_t j = 0; j < i; j++) {
            if (!graph_add_edge(g, v, j + 1)) {
                graph_free(g);
                return NULL;
            }
        }
    }

    return g;
}

uint32_t graph_get_nb_edges(graph_t * g) {
    return (uint32_t) vec_size(g->edges);
}

void graph_get_edges(graph_t * g, uint32_t * edges) {
    uint32_t n = vec_size(g->edges);
    for (uint32_t i = 0; i < n; i++) {
        edge_t * e = (edge_t *) vec_get(g->edges, i);
        edges[2*i] = e->from_vertex;
        edges[2*i + 1] = e->to_vertex;
    }
}

uint32_t graph_get_nb_vertices(graph_t * g) {
    return g->nb_vertices;
}
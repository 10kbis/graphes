#ifndef _GRAPH_H_
#define _GRAPH_H_

#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct {
    uint32_t from_vertex;
    uint32_t to_vertex;
} edge_t;

/* Opaque structure */
typedef struct graph graph_t;

graph_t * graph_create();
void graph_free(graph_t * g);
uint32_t graph_add_vertex(graph_t * g);
bool graph_add_edge(graph_t * g, uint32_t i, uint32_t j);
int graph_evolve(graph_t * g, unsigned m, unsigned n);
void graph_save_graphviz(graph_t * g, char * path, uint32_t threshold);
graph_t * graph_create_kn(uint32_t n);
uint32_t graph_get_nb_edges(graph_t * g);
void graph_get_edges(graph_t * g, uint32_t * edges);
uint32_t graph_get_nb_vertices(graph_t * g);

#endif
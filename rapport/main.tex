\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{enumitem}
\usepackage{pgfplotstable}
\usepackage{booktabs}
\usepackage{todonotes}
\usepackage{amsmath}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{tikz}
\usepackage{wrapfig}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
}
\usepackage{biblatex}
\addbibresource{cite.bib}

\usepackage{graphicx}
\graphicspath{ {images/} }

\title{Théorie des graphes}
\author{Timmermans Matthieu - s134502}

\begin{document}

\maketitle

\section{Modèle théorique}
Le modèle de Barabási-Albert \cite{barabasi} est un modèle de génération de graphe aléatoire dit \textit{scale-free}. Il est composé deux ingrédients
\begin{itemize}
    \item{\textit{Croissance}} Le graphe évolue, de nouveaux noeuds sont ajoutés au fur et à mesure du temps. $m_0$ est le nombre de noeuds de départ et $m$ est le nombre d'arêtes à créer pour chaque nouveau noeud ajouté au graphe.
    \item{\textit{Attachement préférentiel}} La probabilité $\Pi$ qu'un nouveau noeud soit connecté à un autre noeud $i$ dépend du nombre d'arêtes possédées par ce noeud.
    $$\Pi(k_i) = \frac{k_i}{\sum_j k_j}$$
    où $k_i$ est le degré du noeud $i$.
\end{itemize}

\section{Implémentation}
\subsection{Structure des données}
Le graphe est représenté par un nombre de sommets $n$ et une liste de tuple $(i, j)$ ($0 < i,j \leq n$) représentant une arête. Les sommets ne sont donc pas explicitement sauvegardés.

\subsection{Algorithmes}
Une implémentation naïve serait de calculer la distribution des probabilités pour tous les noeuds à chaque ajout en suivant simplement la formule $\Pi$ ci dessus. Cette solution aurait une complexité algorithmique de $O(n^2)$ (pour un $m$ constant) où $n$ est le nombre de noeuds à ajouter au total.

Une meilleur solution est de sauver les arêtes dans un tableau et d'en tirer une aléatoirement en choisissant le noeud de départ de cette arête comme étant le noeud auquel sera connecté le nouveau noeud.

Comme il y a $k_i$ arêtes dans le tableau pour le noeud $i$ et que la taille du tableau est $\sum_j k_j$, on retombe bien sur $\Pi(k_i)$. Pour que cette solution fonctionne avec des graphes non orienté, il faut que les deux arêtes $(i, j)$ et $(j, i)$ soit dans le tableau.

En choisissant aléatoirement de manière répétée plusieurs arêtes, on peut s'attendre à avoir des problèmes à cause du paradoxe des anniversaires. Cependant puisque, $m$, le nombre d'arêtes à choisir, reste constant mais que le nombre d'arêtes augmente au fur et à mesure, la probabilité de choisir deux fois la même arête diminue.

L'algorithme décrit si dessus a une complexité de $O(n)$ si l'accès à un élément de la liste des arêtes est en $O(1)$.

\subsection{Choix aléatoire}
Dans le langage C, tirer un nombre aléatoire se fait usuellement à l'aide de la fonction $rand()$. Cependant cette fonction ne garantit pas de retourner un nombre suffisamment grand que pour être utilisé pour tirer une arête aléatoire dans de très grand graphes \cite{cmanuel}.

Pour palier à ce problème, l'algorithme Xorshift \cite{xorshift} a été implémenté dans sa version 32-bits.

\subsection{Versions}
Deux versions du programme sont fournies, la première est un simple utilitaire en mode console. Son usage est disponible en appelant l'utilitaire sans arguments. Cette version permet d'exporter le graphe final en fichier Graphviz.

La seconde version est une page web (disponible à l'adresse \url{https://10kbis.gitlab.io/graphes/}). Cette version a été uploadée sur GitLab Pages pour une plus grande facilité d'utilisation car elle requiert un serveur web.

La version web possède des outils de visualisation plus complets. La visualisation du graphe est faite à l'aide de la libraire \textit{vis.js} \cite{visjs}. La compilation du code C en WebAssembly est faite à l'aide du compilateur emscriptem \cite{emscriptem}.

Emscriptem compile la librairie \textit{graph.c} en fichier WebAssembly \textit{graph.wasm} et Javascript \textit{graph.js}. Le WebAssembly est le code à proprement parlé, le Javascript sert à charger ce code ainsi qu'à exporter les fonctions nécessaires. Ces deux fichiers sont automatiquement générés et ne sont pas modifiés manuellement. La page web est contenue dans le fichier \textit{index.html}.

\section{Benchmark}

Le benchmark suivant test le temps d'exécution en fonction de différents valeurs pour les paramètres $n$ et $m$. Le nombre de noeuds au début de la génération, $m_0$, n'a pas d'influence lorsque $n$ devient grand, il n'a donc pas été pris en compte.

L'utilitaire console a été utilisé pour générer les graphes en désactivant l'écriture vers le fichier de sortie pour ne pas être impacté par les accès disques.

\begin{tikzpicture}
\begin{axis}[
    title={Temps de génération en fonction $n$ et $m$},
    xlabel={Nombre de noeuds générés [$n$]},
    ylabel={Temps [sec]},
    xmin=0, xmax=100000000,
    ymin=0, ymax=100,
    xtick={1,10,100,1000,10000,100000,1000000,10000000},
    ytick={0.001,0.01,0.1,1,10,100},
    legend pos=north west,
    ymajorgrids=true,
    grid style=dashed,
    xmode=log,
    ymode=log,
]
 
\addplot[
    color=blue,
    mark=square,
    ]
    coordinates {
        (100,       0.000)
        (1000,      0.002)
        (10000,     0.005)
        (100000,    0.027)
        (1000000,   0.304)
        (10000000,  3.240)
    };
 
\addplot[
    color=red,
    mark=square,
    ]
    coordinates {
        (100,       0.000)
        (1000,      0.003)
        (10000,     0.010)
        (100000,    0.079)
        (1000000,   0.911)
        (10000000,  10.72)
    };
 
\addplot[
    color=red,
    mark=square,
    ]
    coordinates {
        (100,       0.000)
        (1000,      0.004)
        (10000,     0.020)
        (100000,    0.196)
        (1000000,   2.246)
        (10000000,  27.58)
    };
\legend{$m = 1$, $m = 3$, $m = 7$}
\end{axis}
\end{tikzpicture}

Comme on peut le voir, l'algorithme est linéaire en fonction de $n$ et permet de générer un très grand nombre de noeuds en peu de temps. Changer le paramètre $m$ n'a pas d'influence sur la linéarité.

\section{Compilation}
La compilation se fait à l'aide du fichier \textit{makefile}.

L'utilitaire est compilé à l'aide d'un simple \colorbox{lightgray}{make}.

La compilation de la version web nécessite \textit{emscript} et \textit{vis.js}. Une version compilée est déjà fournie pour ne pas devoir installer ces dépendances juste pour tester. La compilation se fait aussi à l'aide de make avec \colorbox{lightgray}{make web}. Pour visualiser localement la page web, il faut servir le dossier du projet à l'aide d'un serveur web.

\section{Exemples}

\begin{figure}[H]
    \centering
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{example50_k3.png}
        \caption{$n = 50$, $m = 1$, graphe de départ $k_3$}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{example_200_k3.png}
        \caption{$n = 200$, $m = 1$, graphe de départ $k_3$}
    \end{subfigure}%
\end{figure}
\begin{figure}[H]
    \centering
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{example3000_k5.png}
        \caption{$n = 3000$, $m = 1$, graphe de départ $k_5$}
    \end{subfigure}%
\end{figure}

\printbibliography
    
\end{document}

#include "vec.h"
#include <stdlib.h>

struct vec {
    size_t size;
    size_t capacity;
    void ** content;
    void (* free_elem_func)(void * ptr);
};


vec_t * vec_create(free_func_t free_elem_func) {
    size_t capacity = 2;

    void ** content = calloc(capacity, sizeof(void *));
    if (!content) {
        return NULL;
    }

    vec_t * vec = malloc(sizeof(vec_t));
    if (!vec) {
        free(content);
        return NULL;
    }
    vec->size = 0;
    vec->capacity = capacity;
    vec->content = content;
    vec->free_elem_func = free_elem_func;

    return vec;
}


void vec_free(vec_t * vec) {
    if (!vec) {
        return;
    }
    if (vec->free_elem_func) {
        for (size_t i = 0; i < vec->size; i++) {
            vec->free_elem_func(vec->content[i]);
        }
    }
    free(vec->content);
    free(vec);
}


bool vec_add(vec_t * vec, void * elem) {
    if (vec->size == vec->capacity) {
        void ** new_content = realloc(vec->content, vec->capacity * 2 * sizeof(void *));
        if(!new_content) {
            return false;
        }

        vec->content = new_content;
        vec->capacity *= 2;
    }

    vec->content[vec->size++] = elem;
    return true;
}

void * vec_get(const vec_t * vec, size_t index) {
    return vec->content[index];
}

void * vec_pop(vec_t * vec) {
    if (vec->size == 0) {
        return NULL;
    }
    return vec->content[--(vec->size)];
}

size_t vec_size(const vec_t * vec) {
    return vec->size;
}

void vec_sort(vec_t * vec, cmp_func_t cmp_func) {
    qsort(vec->content, vec->size, sizeof(void *), cmp_func);
}


void vec_filter(vec_t * vec, filter_func_t filter_func, void * data, size_t max) {
    size_t current = 0;
    size_t removed = 0;

    for (size_t i = 0; i < vec->size; i++) {
        if (max == 0 || removed < max) {
            bool remove = filter_func(vec->content[i], data);
            if (remove) {
                removed++;
            } else {
                vec->content[current] = vec->content[i];
                current++;
            }
        } else {
            vec->content[current] = vec->content[i];
            current++;
        }
    }

    vec->size = current;
}


bool vec_contains(vec_t * vec, cmp_func_t cmp_func, void * elem) {
    for (size_t i = 0; i < vec->size; i++) {
        if (cmp_func(vec->content[i], elem) == 0) {
            return true;
        }
    }

    return false;
}

bool vec_empty(vec_t * vec) {
    return vec->size == 0;
}

vec_t * vec_copy(vec_t * vec, void * (* copy_func)(void * elem)) {
    vec_t * new_vec = malloc(sizeof(vec_t));
    if (!new_vec) {
        return NULL;
    }

    new_vec->content = malloc(vec->capacity * sizeof(void *));
    if (!new_vec->content) {
        free(new_vec);
        return NULL;
    }

    for (size_t i = 0; i < vec->size; i++) {
        if (copy_func) {
            new_vec->content[i] = copy_func(vec->content[i]);
        } else {
            new_vec->content[i] = vec->content[i];
        }
    }

    new_vec->size = vec->size;
    new_vec->capacity = vec->capacity;

    // If elements are not copied then the new vector is referencing
    // the elements from the old vector. The new vector must not free
    // its elements when it is destroyed
    if (copy_func) {
        new_vec->free_elem_func = vec->free_elem_func;
    } else {
        new_vec->free_elem_func = NULL;
    }

    return new_vec;
}

void vec_clear(vec_t * vec) {
    if (vec->free_elem_func) {
        for (size_t i = 0; i < vec->size; i++) {
            vec->free_elem_func(vec->content[i]);
        }
    }
    vec->size = 0;
}

void vec_remove(vec_t * vec, size_t i) {
    if (vec->free_elem_func) {
        vec->free_elem_func(vec->content[i]);
    }

    for (;i < vec->size - 1; i++) {
        vec->content[i] = vec->content[i + 1];
    }

    vec->size--;
}

void vec_swap(vec_t * vec, size_t i, size_t j) {
    if (i < vec->size && j < vec->size) {
        void * tmp = vec->content[i];
        vec->content[i] = vec->content[j];
        vec->content[j] = tmp;
    }
}
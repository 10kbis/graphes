#include "utils.h"

static uint32_t rand32_state = 2463534242;

uint32_t rand32() {
	rand32_state ^= rand32_state << 13;
	rand32_state ^= rand32_state >> 17;
	rand32_state ^= rand32_state << 5;
	return rand32_state;
}

void srand32(uint32_t seed) {
    rand32_state = seed;
}


static unsigned number_bits(unsigned x) {
    if (x <= 1) {
        return 1;
    }

    unsigned bits = 0;
    while (x) {
        x >>= 1;
        bits += 1;
    }
    return bits;
}

uint32_t randint(uint32_t min, uint32_t max) {
    uint32_t range = max - min;
    uint32_t r;

    do {
        r = rand32();
        r = r >> (32 - number_bits(range));
    } while (r > range);

    return r + min;
}


int cmp_size_t(size_t * a, size_t * b) {
    if (*a > *b) {
        return 1;
    } else if (*a == *b) {
        return 0;
    } else {
        return -1;
    }
}